package com.hackathon5a.tradeservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.hackathon5a.tradeservice.model.Trade;
import com.hackathon5a.tradeservice.service.TradeService;

@CrossOrigin
@RestController
@RequestMapping("/api/trade")
public class TradeController {
    @Autowired
    TradeService service;

    @GetMapping(value = "/checkBalance/{user_id}/{tradevalue}")
    public Boolean checkBalance(@PathVariable int user_id, @PathVariable Double tradevalue) {
        return service.checkBalance(user_id, tradevalue);
    }

    @GetMapping(value = "/checkequityinportfolio/{user_id}/{ticker_symbol}/{quantity}")
    public Boolean checkequityinportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol,
            @PathVariable Double quantity) {
        return service.checkequityinportfolio(user_id, ticker_symbol, quantity);
    }

    @GetMapping(value = "/get")
    public List<Trade> getAllTrades() {
        return service.getAllTrades();
    }

    @GetMapping(value = "/get/{id}")
    public Trade getTradeById(@PathVariable("id") int id) {
        return service.getTrade(id);
    }

    @PostMapping(value = "/add")
    public Trade addTrade(@RequestBody Trade trade) {
        return service.sendTradeToExchange(trade);
    }

    @PostMapping(value = "/getresponse")
    public Trade getresponse(@RequestBody Trade trade) {
        return service.getResponseFromExchange(trade);
    }

    // @PutMapping(value = "/")
    // public Trade editTrade(@RequestBody Trade trade) {
    // return service.saveTrade(trade);
    // }

    @DeleteMapping(value = "/delete/{id}")
    public int deleteOrder(@PathVariable int id) {
        return service.deleteTrade(id);
    }
}
