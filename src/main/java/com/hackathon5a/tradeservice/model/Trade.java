package com.hackathon5a.tradeservice.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.Id;

import java.sql.Timestamp;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Builder
@Data
@Getter
@Setter
public class Trade {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int user_id;
    private int trade_id;
    private Timestamp trade_time;
    private String ticker_symbol;
    private int order_side;
    private String order_type;
    private double quantity;
    private double price;
    private int status_code;

    public Trade() {
    }

    public Trade(int user_id, int trade_id, Timestamp trade_time, String ticker_symbol, int order_side, String order_type, double quantity, double price, int status_code) {
        this.user_id = user_id;
        this.trade_id = trade_id;
        this.trade_time = trade_time;
        this.ticker_symbol = ticker_symbol;
        this.order_side = order_side;
        this.order_type = order_type;
        this.quantity = quantity;
        this.price = price;
        this.status_code = status_code;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTrade_id() {
        return trade_id;
    }

    public void setTrade_id(int trade_id) {
        this.trade_id = trade_id;
    }

    public Timestamp getTrade_time() {
        return trade_time;
    }

    public void setTrade_time(Timestamp trade_time) {
        this.trade_time = trade_time;
    }

    public String getTicker_symbol() {
        return ticker_symbol;
    }

    public void setTicker_symbol(String ticker_symbol) {
        this.ticker_symbol = ticker_symbol;
    }

    public int getOrder_side() {
        return order_side;
    }

    public void setOrder_side(int order_side) {
        this.order_side = order_side;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }
}
