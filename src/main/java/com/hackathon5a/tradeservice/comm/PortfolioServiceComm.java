package com.hackathon5a.tradeservice.comm;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "portfolio-service", url = "portfolio-service-hackathon.punedevopsa4.conygre.com")
public interface PortfolioServiceComm {

    @GetMapping(value = "/api/portfolio/checkequityinportfolio/{user_id}/{ticker_symbol}/{quantity}")
    Boolean checkequityinportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol,
            @PathVariable Double quantity);

    @GetMapping(value = "/api/portfolio/addtoportfolio/{user_id}/{ticker_symbol}/{quantity}/{avg_price}")
    Boolean addtoportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol, @PathVariable Double quantity,
            @PathVariable Double avg_price);

    @GetMapping(value = "/api/portfolio/removefromportfolio/{user_id}/{ticker_symbol}/{quantity}/{avg_price}")
    Boolean removefromportfolio(@PathVariable int user_id, @PathVariable String ticker_symbol,
            @PathVariable Double quantity, @PathVariable Double avg_price);
}
