package com.hackathon5a.tradeservice.comm;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "money-service", url = "money-service-hackathon.punedevopsa4.conygre.com")
public interface MoneyServiceComm {

    @GetMapping(value = "/api/money/checksufficientbalance/{user_id}/{tradevalue}")
    Boolean checkBalance(@PathVariable int user_id, @PathVariable Double tradevalue);

    @GetMapping(value = "/api/money/add/{user_id}/{money}")
    String add(@PathVariable int user_id, @PathVariable Double money);

    @GetMapping(value = "/api/money/withdraw/{user_id}/{money}")
    String withdraw(@PathVariable int user_id, @PathVariable Double money);
}
