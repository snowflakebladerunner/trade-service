package com.hackathon5a.tradeservice.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon5a.tradeservice.comm.MoneyServiceComm;
import com.hackathon5a.tradeservice.comm.PortfolioServiceComm;
import com.hackathon5a.tradeservice.model.Trade;
import com.hackathon5a.tradeservice.repository.Traderepository;

@Service
public class TradeService {
    @Autowired
    private Traderepository repository;
    @Autowired
    private MoneyServiceComm moneyServiceComm;
    @Autowired
    private PortfolioServiceComm portfolioservicecomm;

    private int percentFailures = 10;

    ArrayList<Trade> trade_list = new ArrayList<>();

    public Boolean checkBalance(int user_id, Double tradevalue) {
        return moneyServiceComm.checkBalance(user_id, tradevalue);
    }

    public Boolean checkequityinportfolio(int user_id, String ticker_symbol, Double quantity) {
        return portfolioservicecomm.checkequityinportfolio(user_id, ticker_symbol, quantity);
    }

    public List<Trade> getAllTrades() {
        return repository.getAllTrades();
    }

    public Trade getTrade(int id) {
        return repository.getTradeById(id);
    }

    public Trade editTrade(Trade trade) {
        return repository.editTrade(trade);
    }

    public Trade newTrade(Trade trade) {
        trade.setTrade_time(new Timestamp(System.currentTimeMillis()));
        return repository.addTrade(trade);
    }

    public int deleteTrade(int id) {
        return repository.deleteTrade(id);
    }

    public Trade sendTradeToExchange(Trade trade) {
        Double tradevalue = trade.getPrice() * trade.getQuantity();

        // if buying
        if (trade.getOrder_side() == 0) {
            if (checkBalance(trade.getUser_id(), tradevalue)) {
                in_request(trade);
                trade.setStatus_code(1);
                newTrade(trade);
                moneyServiceComm.withdraw(trade.getUser_id(), tradevalue);
                return trade;
            } else {
                return null;
            }
        }
        // if selling
        else {
            if (checkequityinportfolio(trade.getUser_id(), trade.getTicker_symbol(), trade.getQuantity())) {
                in_request(trade);
                trade.setStatus_code(1);
                newTrade(trade);
                moneyServiceComm.add(trade.getUser_id(), tradevalue);
                return trade;
            } else {
                return null;
            }
        }

    }

    public Trade getResponseFromExchange(Trade trade) {
        Double tradevalue = trade.getPrice() * trade.getQuantity();

        // if buying
        if (trade.getOrder_side() == 0) {
            // if failed refund money
            if (trade.getStatus_code() == 3) {
                moneyServiceComm.add(trade.getUser_id(), tradevalue);
                editTrade(trade);
                return trade;
            }

            if (checkBalance(trade.getUser_id(), tradevalue)) {
                editTrade(trade);
                portfolioservicecomm.addtoportfolio(trade.getUser_id(), trade.getTicker_symbol(), trade.getQuantity(),
                        trade.getPrice());
                return trade;
            } else {
                return null;
            }
        }
        // if selling
        else {
            // if failed withdraw money
            if (trade.getStatus_code() == 3) {
                moneyServiceComm.withdraw(trade.getUser_id(), tradevalue);
                editTrade(trade);
                return trade;
            }

            if (checkequityinportfolio(trade.getUser_id(), trade.getTicker_symbol(), trade.getQuantity())) {
                editTrade(trade);
                portfolioservicecomm.removefromportfolio(trade.getUser_id(), trade.getTicker_symbol(),
                        trade.getQuantity(), trade.getPrice());
                return trade;
            } else {
                return null;
            }
        }
    }

    public void in_request(Trade trade) {
        trade_list.add(trade);

        ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);

        exec.schedule(new Runnable() {
            public void run() {
                out_response();
            }
        }, 2, TimeUnit.SECONDS);
    }

    public Trade out_response() {

        Trade selectedtrade;

        int index = new Random().nextInt(trade_list.size());

        int dice = new Random().nextInt(100);

        if (dice > percentFailures) {
            selectedtrade = trade_list.get(index);
            selectedtrade.setStatus_code(2);
        } else {
            selectedtrade = trade_list.get(index);
            selectedtrade.setStatus_code(3);
        }

        trade_list.remove(index);

        return getResponseFromExchange(selectedtrade);
    }

}
