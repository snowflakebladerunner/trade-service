package com.hackathon5a.tradeservice.repository;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

import com.hackathon5a.tradeservice.model.Trade;

@Component
public interface Traderepository {
    public List<Trade> getAllTrades();

    public Trade getTradeById(int id);

    public Trade editTrade(Trade trade);

    public int deleteTrade(int id);

    public Trade addTrade(Trade trade);

    public class IdGenerator {
        public static int generateUniqueId() {
            UUID idOne = UUID.randomUUID();
            String str = "" + idOne;
            int uid = str.hashCode();
            String filterStr = "" + uid;
            str = filterStr.replaceAll("-", "");
            return Integer.parseInt(str);
        }
    }
}
