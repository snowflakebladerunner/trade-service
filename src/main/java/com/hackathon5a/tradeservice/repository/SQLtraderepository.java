package com.hackathon5a.tradeservice.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.hackathon5a.tradeservice.model.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class SQLtraderepository implements Traderepository {

    @Autowired
    JdbcTemplate template;

    @Override
    public List<Trade> getAllTrades() {
        String sql = "SELECT user_id, trade_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code FROM trade ORDER BY trade_time";
        return template.query(sql, new TradeRowMapper());
    }

    @Override
    public Trade getTradeById(int id) {
        String sql = "SELECT user_id, trade_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code FROM trade WHERE trade_id=?";
        return template.queryForObject(sql, new TradeRowMapper(), id);
    }

    @Override
    public Trade editTrade(Trade trade) {
        String sql = "UPDATE trade SET trade_time = ?, ticker_symbol = ?, order_side = ?, order_type = ?, quantity = ?, price = ?, status_code = ? WHERE trade_id = ?";
        template.update(sql, trade.getTrade_time(), trade.getTicker_symbol(), trade.getOrder_side(),
                trade.getOrder_type(), trade.getQuantity(), trade.getPrice(), trade.getStatus_code(),
                trade.getTrade_id());
        return trade;
    }

    @Override
    public int deleteTrade(int id) {
        String sql = "DELETE FROM trade WHERE trade_id = ?";
        template.update(sql, id);
        return id;
    }

    @Override
    public Trade addTrade(Trade trade) {
        trade.setTrade_id(IdGenerator.generateUniqueId());
        String sql = "INSERT INTO trade(user_id, trade_id, trade_time, ticker_symbol, order_side, order_type, quantity, price, status_code) "
                + "VALUES(?,?,?,?,?,?,?,?,?)";
        template.update(sql, trade.getUser_id(), trade.getTrade_id(), trade.getTrade_time(), trade.getTicker_symbol(),
                trade.getOrder_side(), trade.getOrder_type(), trade.getQuantity(), trade.getPrice(),
                trade.getStatus_code());
        return trade;
    }
}

class TradeRowMapper implements RowMapper<Trade> {
    @Override
    public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Trade(rs.getInt("user_id"), rs.getInt("trade_id"), rs.getTimestamp("trade_time"),
                rs.getString("ticker_symbol"), rs.getInt("order_side"), rs.getString("order_type"),
                rs.getDouble("quantity"), rs.getDouble("price"), rs.getInt("status_code"));
    }
}
